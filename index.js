// console.log("Hello World")

// [section] functions
// functions in js are lines/block of codes that tell our device/application to perform a certain task when called/invoked
// They are also used to prevent repeating lines of codes that perform the same task/function

// Function decleration
	// function statement defines a function with parameters

		// function keyword - use to define a js function
		// function name - is used to we are able to call a declared fucntion
		// function code block ({}) -the statement which compromise the body of the function. This is where the code to be excuted
// function declaration // function name
// function name requires open and close parenthesis beside it
function printName(){ /*code block*/
	console.log("My name is John") /*function statement*/
}; /*delimeter*/

printName();

/*----------------*/
// [hoisting]
// IS JS BEHAVIOR FOR CERTIAN VARIABLE AND FUNCTIONS TO RUN OR USE BEFORE THE FUNCITON
declaredFunction();
								// make sure the function is existing whenever we call a fucntion

function declaredFunction(){
	console.log("Hello World")
}

declaredFunction();
declaredFunction();

/*-----------------------------*/
// FUNCTION EXPRESSION
// a fucntion can also be stored in a variable. That is called as function expression

// variableFunction();

let variableFunction = function(){
console.log("Hello World")
};

variableFunction();

let functionExpression = function funcName(){
	console.log("Hello from the other side!")
};

functionExpression();
// only call the function stored in the variable, call the variable name not the function name

console.log("-------------------------");

console.log("Reassigning Functions");

declaredFunction();

declaredFunction = function(){
	console.log("updated declaredFunction")
};

declaredFunction();

functionExpression = function(){
	console.log("updated funcExpression")
}

functionExpression();

// constant fucntion
const constantFunction = function(){
	console.log("Initialized with const")
}

constantFunction();

// constantFunction = function() {
// 	console.log("New value")
// }

// constantFunction(); /**/ cant change cos its constant
// scope is the accesibility / visibility of a variables in the code

/*
	js variables has 3 types of scope
	1/ local scope
	2. global scope
	3. function scope
*/
console.log("------------------");
console.log("[Function scoping]");

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

// console.log(localVar);



let globalVar = "Mr. Worldwide"
console.log(globalVar);

{
	console.log(globalVar);
}

// function scoping
// variables defined inside a function are not accessible/visible outside the fucntion
// variables declared with var, let, and const are quite similar when declared inside a fucntion

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John"
	let functionLet = "Jane"

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// error-coz its outside the function
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested functions
	// you can create another fucntion inside a function, called nested fucntion

function myNewFunction(){
	let name = "Jane"

	function nestedFunction(){
		let nestedName = "John"
		console.log(name);
	}

	nestedFunction();
	console.log(name);
	console.log(nestedFunction)
}

myNewFunction();

// Global scoped variable
let globalName = "Zuitt";

function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);

}

myNewFunction2();


// alert () makes alert for our browser page to show information to our users
// alert("Warning don't come inside");

function showSampleAlert(){
	alert("Hello, User");
}

showSampleAlert();

// alert messages inside a function will only execute whenever we call the function


console.log("I will only log in the console when the alert is dismissed");


// notes on the use of alert();
	// show only an alert for short dialogs/messages to the user
	// do not overload of text the alert because the program js has to wait for it to be dismissed before proceeding

// [Prompt]
	// prompt () allow us to show small window at the top of the browser input.

// let samplePrompt = prompt ("Enter your full name")
// console.log(samplePrompt);

// console.log(typeof samplePrompt);

// console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");


console.log("Hello, " + firstName  + lastName + "!")
console.log("Welcome to my page")

}

printWelcomeMessage();


// [section] function naming convention

// function name should be definitive of the task it will perform. it usually contains a verb


function getCourses(){
	let courses = ["Science 101", "math 101", "english 101"]
	console.log(courses)
}

getCourses();

// avoid generic names to avoid confusion within our code.

function get(){
	let name = "Jamie"
	console.log(name)
}

get();

// avoid pointless and inappropriate function names, ex: foo, bar, etc.

function foo(){
	console.log(25%5)
}

foo();

// name your function in small caps. Follow camelCase when naming variables and functions

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000")
}

displayCarInfo();





